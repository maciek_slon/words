from matplotlib import pyplot as plt
import sys
import numpy as np

labels = []
accs = []
rels = []

for line in open(sys.argv[1]):
    sp = line.strip().split(";")
    if len(sp) < 2:
        continue
    label = sp[0]
    labels.append(label)
    acc = float(sp[1])
    accs.append(acc)
    rel = acc / accs[0]
    rels.append(rel)
    n = (len(sp) - 2) // 2
    tra = [float(x) for x in sp[2:2+n]]
    plt.plot(tra, label=label, linewidth=1)
    plt.xlim([0, n-1])
    plt.ylim([0.5, 1.0])
    plt.xticks(np.arange(4, n, step=5), np.arange(5, n+1, step=5))
    plt.grid(color=(0.7, 0.7, 0.7), linestyle="--", linewidth=0.5)
    plt.xlabel("epoch")
    plt.ylabel("training accuracy")
    plt.legend()

plt.savefig(sys.argv[1][:-4] + ".pdf")

plt.close()
plot = plt.bar(range(len(labels)), accs, tick_label=labels, color=(0.8, 0.8, 0.8))
plt.ylim([0.5, 1.0])
plt.ylabel("testing accuracy")

# Add the data value on head of the bar
for value in plot:
    height = value.get_height()
    plt.text(value.get_x() + value.get_width()/2.,
             0.5,'%.3f' % height, ha='center', va='bottom')

plt.savefig(sys.argv[1][:-4] + "_acc.pdf")

plt.close()
plot = plt.bar(range(len(labels)), rels, tick_label=labels, color=(0.8, 0.8, 0.8))
plt.ylim([0.5, 1.0])
plt.ylabel("relative testing accuracy")

# Add the data value on head of the bar
for value in plot:
    height = value.get_height()
    plt.text(value.get_x() + value.get_width()/2.,
             0.5,'%.3f' % height, ha='center', va='bottom')

plt.savefig(sys.argv[1][:-4] + "_rel.pdf")
