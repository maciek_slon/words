from matplotlib import pyplot as plt
import sys
import numpy as np


accs = {}
nmis = {}
labels = []

for line in open(sys.argv[1]):
    sp = line.strip().split(";")
    if len(sp) < 2:
        continue
    label = sp[0]
    acc = float(sp[1])
    nmi = float(sp[2])
    if not label in labels:
        labels.append(label)
        accs[label] = []
        nmis[label] = []

    accs[label].append(acc)
    nmis[label].append(nmi)

for lbl in labels:
    accs[lbl] = np.array(accs[lbl])
    nmis[lbl] = np.array(nmis[lbl])

    sid = np.argsort(accs[lbl])
    accs[lbl] = np.mean(accs[lbl][sid][1:-1])
    nmis[lbl] = np.mean(nmis[lbl][sid][1:-1])

#for lbl in labels:
#    accs[lbl] = accs[lbl] / accs["id"]
#    nmis[lbl] = nmis[lbl] / nmis["id"]


plot_lbls = ["id"]
plot_accs = [1.0]
plot_nmis = [1.0]

for lbl in labels:
    if lbl == "id":
        continue

    plot_lbls.append(lbl)
    plot_accs.append(accs[lbl] / accs["id"])
    plot_nmis.append(nmis[lbl] / nmis["id"])

plot = plt.bar(range(len(plot_lbls)), plot_accs, tick_label=plot_lbls, color=(0.8, 0.8, 0.8))
plt.ylim([0.0, 1.3])
plt.ylabel("relative accuracy vs no transform")

# Add the data value on head of the bar
for value in plot:
    height = value.get_height()
    plt.text(value.get_x() + value.get_width()/2.,
             0.0,'%.3f' % height, ha='center', va='bottom')

plt.savefig(sys.argv[1][:-4] + "_acc.pdf")


plt.close()

plot = plt.bar(range(len(plot_lbls)), plot_nmis, tick_label=plot_lbls, color=(0.8, 0.8, 0.8))
plt.ylim([0.0, 1.3])
plt.ylabel("relative NMI vs no transform")

# Add the data value on head of the bar
for value in plot:
    height = value.get_height()
    plt.text(value.get_x() + value.get_width()/2.,
             0.0,'%.3f' % height, ha='center', va='bottom')

plt.savefig(sys.argv[1][:-4] + "_nmi.pdf")
