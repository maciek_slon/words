import sys
import os
from os import listdir
from os.path import isfile, join
from numpy import array
import numpy as np

mypath = sys.argv[1]
files = [f for f in listdir(mypath) if isfile(join(mypath, f)) and f.endswith("txt")]


for f in files:
    nan = -1
    lines = open(join(mypath, f)).readlines()
    trans = f.split('_')[0]
#    acc = eval(lines[0])[1]
    cm = eval(' '.join(lines[0:-1]))[1]
    cnt = cm.sum(axis=1)
    accs = [ cm[i,i] / cnt[i] for i in range(cm.shape[0]) ]
#    print(accs)
    acc = np.mean(accs, axis=-1)
    hist = eval(lines[-1])
    print(f'{trans};' + str(acc) + ';' + ';'.join([str(x) for x in hist['acc']]) + ';' + ';'.join([str(x) for x in hist['loss']]))


