## Format plików csv

### Klasyfikacja

Pliki csv dla testów klasyfikacji mają format:

id ; test accuracy ; N * training accuracy ; N * training loss

N to liczba epok

### STC

Pliki csv dla zadania STC mają format:

id ; accuracy ; nmi
