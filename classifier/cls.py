# -*- coding: utf-8 -*-

import numpy as np
import tensorflow.keras as keras


###################################################################################################
#
# Ładowanie danych 
#
# Funkcje powinny zwracać (X_train, X_test, Y_train, Y_test)
#
##################################################################################################

def load_dataset(fname, words_col, label_col, n_words, skip_rows = [], remove_stops = True, split_ratio=0.1):
  '''
    Ładowanie danych z pliku CSV
    - words_col: indeks kolumny z tekstem
    - label_col: indeks kolumny z etykietami
  '''
  import nltk
  from nltk.corpus import stopwords
  from nltk.tokenize import RegexpTokenizer
  from sklearn.preprocessing import OneHotEncoder
  from sklearn.model_selection import train_test_split
  from pandas.api.types import is_string_dtype
 

  nltk.download('stopwords')
  nltk.download('punkt')

  loadcols = [words_col, label_col]
  namecols = ['words', 'labels']
  if words_col > label_col:
    loadcols.reverse()
    namecols.reverse()

  dane = pd.read_csv(fname, encoding='utf-8', usecols=loadcols, names=namecols, skiprows=skip_rows)
  words = dane['words']
  Y_train = dane[['labels']]

#  for tp in Y_train.itertuples():
#      print(tp[1])

#  exit()

  X_train = []
  stop_words = set(stopwords.words('english'))
  tokenizer = RegexpTokenizer(r'\w+')
  for row in words:
      row = tokenizer.tokenize(row)
      filtered_sentence = []
      for w in row:
          if not (remove_stops and w in stop_words):
              filtered_sentence.append(w)

      current_chunk_words = []

      for i, word in enumerate(filtered_sentence):
          if i >= n_words:
              break
          current_chunk_words.append(word)

      current_chunk_words += [''] * (n_words - len(current_chunk_words))
      df_length = len(X_train)
      X_train.append(current_chunk_words)

  X_train = np.array(X_train)
  onehot_encoder = OneHotEncoder(sparse=False)
  Y_train = onehot_encoder.fit_transform(Y_train)
  X_test = []
  Y_test = []
  print(onehot_encoder.get_feature_names(['label']))
  if split_ratio > 0:
    X_train, X_test, Y_train, Y_test = train_test_split(X_train, Y_train, test_size=split_ratio, random_state=7654, stratify=Y_train)

  return (X_train, X_test, Y_train, Y_test)

# twitter
#! wget http://cs.stanford.edu/people/alecmgo/trainingandtestdata.zip
#! unzip trainingandtestdata.zip
def load_tweets(fname, word_vectors, total, nwords, nskip, label_col, text_col):
  import csv
  import nltk
  from nltk.tokenize import TweetTokenizer
  from nltk.corpus import stopwords
  from sklearn.preprocessing import OneHotEncoder
  from sklearn.model_selection import train_test_split

  nltk.download('stopwords')


  lineno = 0

  X = []
  Y = []

  tknzr = TweetTokenizer(strip_handles=True, reduce_len=True)

  with open(fname, 'r', newline='', encoding='latin1') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:
      cur_row = []
      if lineno % nskip == 0:
        words = tknzr.tokenize(row[text_col])
        for word in words:
          word = word.lower()
          if word in word_vectors and not (word in stopwords.words('english')):
            cur_row.append(word)
          
        cur_row += [''] * (nwords - len(cur_row))
        X.append(cur_row[:nwords])
        Y.append([int(row[label_col]) / 4])
        
      lineno += 1
      if lineno % 1000 == 0:
          print(f"{lineno/1000}|{total/1000}\r")

  X=np.array(X)
  Y=np.array(Y)
  #onehot_encoder = OneHotEncoder(sparse=False)
  #Y = onehot_encoder.fit_transform(Y)
  X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.1, random_state=0, stratify=Y)

  return X_train, X_test, Y_train, Y_test


def load_imdb():
  from keras.datasets import imdb
  from keras.preprocessing import sequence

  # number of distinct words
  vocabulary_size = 50000

  # number of words per review
  max_review_length = 100

  # load Keras IMDB movie reviews dataset
  (X_train, y_train), (X_test, y_test) = imdb.load_data(num_words=vocabulary_size)
  print('Number of reviews', len(X_train))
  print('Length of first review before padding', len(X_train[0]))
  print('First review', X_train[0])
  print('First label', y_train[0])

  # padding reviews
  X_train = sequence.pad_sequences(X_train, maxlen=max_review_length)
  X_test = sequence.pad_sequences(X_test, maxlen=max_review_length)

  # A dictionary mapping words to an integer index
  word_index = imdb.get_word_index()
  # The first indices are reserved
  word_index = {k:(v+3) for k,v in word_index.items()}
  word_index["<PAD>"] = 0
  word_index["<START>"] = 1
  word_index["<UNK>"] = 2  # unknown
  word_index["<UNUSED>"] = 3
  reverse_word_index = dict([(value, key) for (key, value) in word_index.items()])
  return (X_train, X_test, y_train, y_test)

# display review
def decode_review(text_vec, label):
    return ' '.join([reverse_word_index.get(i, '?') for i in text_vec if i not in [0,1,2,3]]) +  \
           ' --> ' + ('POSITIVE' if label==1 else 'NEGATIVE')


###################################################################################################
#
# Embedingi
#
###################################################################################################

def embed(data, word_vectors, dim, word_dict = None):
  vectors = np.zeros((data.shape[0], data.shape[1], dim))
  for row_id, row in enumerate(data):
    for i, word in enumerate(row):
      if not (word_dict is None):
        word = word_dict[word]
        
      if word in word_vectors:
        vector = word_vectors[word]
        vectors[row_id, i, :] = vector

  return vectors

# Ładowanie oryginalnych embedingów GLOVE
def get_glove():
  import gensim.downloader as api
  word_vectors = api.load("glove-wiki-gigaword-100")  # load pre-trained word-vectors from gensim-data

  word_vectors_dict = {}
  for word in word_vectors.vocab.keys():
    word_vectors_dict[word] = word_vectors[word]

  return word_vectors_dict

# Ładowanie embedingów z pliku tekstowego
def get_from_file(file_path):
  word_vectors = {}
  for line in open(file_path):
    sp = line.strip().split()
    word = sp[0]
    vec = np.array([float(x) for x in sp[1:]])
    word_vectors[word] = vec

  return word_vectors


###################################################################################################
#
# Przetwarzanie embedingów
#
###################################################################################################

def process_vectors(word_vectors, method):
  ret = {}
  for word in word_vectors:
    ret[word] = method(word_vectors[word])
  return ret

def process_fft(vector):
  import scipy
  from scipy import fftpack
  ret = scipy.fftpack.fft(vector)
  ret = abs(ret.real)
  ret = np.real(scipy.fft.ifft(ret))
  return ret

def process_shuffle(vector, seed):
  import random
  random.seed(seed)
  ret = vector.copy()
  random.shuffle(ret)
  return ret

def process_trim(vector, offset):
  vlen = np.array(vector).shape[0]
  ret = np.zeros(vlen)
  ret[offset:vlen-offset] = vector[offset:vlen-offset]
  return ret

def process_reset(vector, nth):
  ret = [x if i % nth != 0 else 0 for i,x in enumerate(vector)]
  return ret
  
def process_random(vector, nth):
  import random
  ret = [x if i % nth != 0 else random.random() for i,x in enumerate(vector)]
  return ret

def process_zero(vector):
  return np.zeros(vector.shape)


###################################################################################################
#
# Struktury sieci do klasyfikacji
#
###################################################################################################

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM, SimpleRNN, Flatten, Bidirectional
from tensorflow.keras.layers import Conv1D, MaxPooling1D, Dropout
import pandas as pd


def ffn_model(input_dim, output_dim = 1):
  model = Sequential()
  model.add(Flatten(input_shape=input_dim))
  model.add(Dense(250, activation='relu'))
  model.add(Dense(1, activation='sigmoid'))
  model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
  return model

def cnn_model(input_dim, output_dim = 1):
  model = Sequential()
  model.add(Conv1D(filters=200, input_shape=input_dim, kernel_size=3, padding='same', activation='relu'))
  model.add(MaxPooling1D(pool_size=2))
  model.add(Flatten())
  model.add(Dense(250, activation='relu'))
  if output_dim == 1:
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
  else:
    model.add(Dense(output_dim, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

  return model

def rnn_model(input_dim, output_dim = 1):
  model = Sequential()
  model.add(SimpleRNN(100, input_shape=input_dim))
  if output_dim == 1:
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
  else:
    model.add(Dense(output_dim, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
  return model

def cnnrnn_model(input_dim, output_dim = 1):
  model = Sequential()
  model.add(Conv1D(filters=32, input_shape=input_dim, kernel_size=3, padding='same', activation='relu'))
  model.add(MaxPooling1D(pool_size=2))
  model.add(SimpleRNN(100))
  if output_dim == 1:
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
  else:
    model.add(Dense(output_dim, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
  return model

def lstm_model(input_dim, output_dim = 1):
  model = Sequential()
  model.add(Bidirectional(LSTM(112, input_shape=input_dim)))
  model.add(Dropout(0.5))
  model.add(Dense(56,name='FC1', activation='relu'))
  if output_dim == 1:
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
  else:
    model.add(Dense(output_dim, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
  return model

models = {
#  'ffn': ffn_model,
#  'cnn': cnn_model,
#  'rnn': rnn_model,
#  'cnnrnn': cnnrnn_model,
  'lstm': lstm_model
}

embed_tfs = {
  'id': lambda v: v,
  'cr100': lambda v: v,
  'qcr100': lambda v: v,
  'fft': lambda v: process_fft(v),
  'shuffle': lambda v: process_shuffle(v, 21357),
  'zero33': lambda v: process_reset(v, 3),
  'random20': lambda v: process_random(v, 5),
  'trim10': lambda v: process_trim(v, 10),
  'random10': lambda v: process_random(v, 10),
  'random25': lambda v: process_random(v, 4),
  'random50': lambda v: process_random(v, 2),
  'random100': lambda v: process_random(v, 1),
  'zero10': lambda v: process_reset(v, 10),
  'zero25': lambda v: process_reset(v, 4),
  'zero50': lambda v: process_reset(v, 2),
  'zero100': lambda v: process_reset(v, 1)
}

def test_model(model, X_test, Y_test):
  from sklearn.metrics import confusion_matrix
  Y_pred = np.argmax(model.predict(X_test, verbose=0), axis=-1)
  Y_exp = np.argmax(Y_test, axis=-1)
  #plot_confusion_matrix_from_data(Y_exp, Y_pred)
  cm = confusion_matrix(Y_exp, Y_pred)
  acc = []
  ymax = cm.max(axis=0)
  counts = cm.sum(axis=0)
  for h in range(0, cm.shape[1]):
      howmuch = ymax[h] / counts[h]
      acc.append(howmuch)
  m = np.mean(acc)
  return m, cm





if __name__ == "__main__":
    # args
    ####################################################################################
    import argparse
    import os

    parser = argparse.ArgumentParser(description='train', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--dataset', choices=['topics', 'news'], required=True)
    parser.add_argument('--data_dir', default="data")
    parser.add_argument('--emb', default='glove')
    parser.add_argument('--save_dir', required=True)
    parser.add_argument('--transformer', nargs='+', required=True)
    parser.add_argument('--epochs', default=20, type=int)
    parser.add_argument('--skip_news', default=0.0, type=float)
    args = parser.parse_args()

    print(args)

    if not os.path.exists(args.save_dir):
        os.makedirs(args.save_dir)

    print("Loading dataset: " + args.dataset + " from " + args.data_dir)
    if args.dataset == 'topics':
        word_count = 50
        word_col = 1
        label_col = 2
        fname = os.path.join(args.data_dir, 'topics/topics.csv')
        print(fname)
        skip_rows=[0]
        remove_stops = True
        split_ratio = 0.1
        X_train, X_test, Y_train, Y_test = load_dataset(fname, word_col, label_col, word_count, skip_rows, remove_stops, split_ratio)
    elif args.dataset == 'news':
        word_count = 50
        word_col = 2
        label_col = 0
        skip_rows=[]
        remove_stops = True
        split_ratio = 0
        fname = os.path.join(args.data_dir, 'ag_news/train.csv')
        X_train, _, Y_train, _ = load_dataset(fname, word_col, label_col, word_count, skip_rows, remove_stops, args.skip_news)
        fname = os.path.join(args.data_dir, 'ag_news/test.csv')
        X_test, _, Y_test, _ = load_dataset(fname, word_col, label_col, word_count, skip_rows, remove_stops, 0)
    else:
        raise Exception("Dataset not found!")

    _, counts = np.unique(Y_train, axis=0, return_counts=True)
    print("Training: ", X_train.shape[0], counts)

    _, counts = np.unique(Y_test, axis=0, return_counts=True)
    print("Testing: ", X_test.shape[0], counts)

    print("Loading embeddings: " + args.emb)
    if os.path.exists(args.emb):
        word_vectors_dict = get_from_file(args.emb)
    elif args.emb == 'glove':
        word_vectors_dict = get_glove()
    else:
        raise Exception("Unknown embeddings: " + args.emb)

    emb_size = word_vectors_dict["the"].shape[0]
    print("Embeddings dim: ", emb_size)


    print("#"*30)
    print("# Let's go")
    print("#"*30)


    for embed_id in args.transformer:
        print(f'Embed transformer: {embed_id}')
        curr_vectors = process_vectors(word_vectors_dict, embed_tfs[embed_id])
        vecs_train = embed(X_train, curr_vectors, emb_size)
        vecs_test = embed(X_test, curr_vectors, emb_size)
        for model_id in models:
            classes = Y_train.shape[1]
            print(f'Model: {model_id}')
            model_fn = models[model_id]
            model = model_fn(vecs_train.shape[1:3], classes)

            hist = model.fit(vecs_train, Y_train, epochs=args.epochs, batch_size=32, verbose=2).history
            if classes == 1:
                sc = model.evaluate(vecs_test, Y_test, verbose=0)
            else:
                sc = test_model(model, vecs_test, Y_test)

        print("*"*30)
        print("Results for", model_id, embed_id)
        print(sc[0])
        print(sc[1])
        print("*"*30)

        with open(f'{args.save_dir}/{embed_id}_{model_id}.txt', 'w') as fout:
            fout.write(str(sc) + '\n')
            fout.write(str(hist))

