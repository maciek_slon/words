DATASET=topics
DATADIR=data
TESTID=${DATASET}_20210613
SAVEDIR=results/${TESTID}
EPOCHS=30
SKIPNEWS=0.75

#python classifier/cls.py --data_dir ${DATADIR} --dataset ${DATASET} --save_dir ${SAVEDIR} --epochs ${EPOCHS} --skip_news ${SKIPNEWS} --transformer id trim10 zero33 fft shuffle --emb glove
python classifier/cls.py --data_dir ${DATADIR} --dataset ${DATASET} --save_dir ${SAVEDIR} --epochs ${EPOCHS} --skip_news ${SKIPNEWS} --transformer qcr100 --emb emb/QCr100corr.txt
#python classifier/cls.py --data_dir ${DATADIR} --dataset ${DATASET} --save_dir ${SAVEDIR} --epochs ${EPOCHS} --skip_news ${SKIPNEWS} --transformer cr100 --emb emb/Cr100.txt

cd results
python res2csv.py ${TESTID} | grep "^id" > ${TESTID}/${TESTID}.csv
python res2csv.py ${TESTID} | grep -v "^id" >> ${TESTID}/${TESTID}.csv

cd ${TESTID}
python ../plots.py ${TESTID}.csv
