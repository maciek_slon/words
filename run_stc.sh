DATADIR=data
DATASET=stackoverflow
TESTID=stc_${DATASET}_20210613
SAVEDIR=results/${TESTID}

for tf in id fft shuffle zero33 random20 trim10
do
	python stc/STC.py --data_dir ${DATADIR} --dataset ${DATASET} --save_dir ${SAVEDIR} --emb glove --transformer ${tf} --reps 5
done

#python stc/STC.py --data_dir ${DATADIR} --dataset ${DATASET} --save_dir ${SAVEDIR} --emb emb/QCr100corr.txt --transformer qcr --reps 5
#python stc/STC.py --data_dir ${DATADIR} --dataset ${DATASET} --save_dir ${SAVEDIR} --emb emb/QCr100corr.txt --transformer random20 --reps 5
#python stc/STC.py --data_dir ${DATADIR} --dataset ${DATASET} --save_dir ${SAVEDIR} --emb emb/QCr100corr.txt --transformer zero33 --reps 5
#python stc/STC.py --data_dir ${DATADIR} --dataset ${DATASET} --save_dir ${SAVEDIR} --emb emb/QCr100corr.txt --transformer trim10 --reps 5
#python stc/STC.py --data_dir ${DATADIR} --dataset ${DATASET} --save_dir ${SAVEDIR} --emb emb/Cr100.txt --transformer cr --reps 5

